const ipcRenderer = require("electron").ipcRenderer;

global.onMainMessage = ipcRenderer.on.bind(ipcRenderer);
global.sendMainMessage = ipcRenderer.send.bind(ipcRenderer);
