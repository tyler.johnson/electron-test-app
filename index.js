const {app, BrowserWindow, ipcMain} = require("electron");

app.on("window-all-closed", app.quit);
app.on("ready", openWindow);
ipcMain.on("cpu-request", returnCPUInfo);
ipcMain.on("network-request", returnNetworkInfo);

function openWindow() {
  let win = new BrowserWindow(
    {
      webPreferences: {
        nodeIntegration: false,
        preload: require("path").join(__dirname, "preload.js")
      },
      frame: false,
      width: 1024,
      height: 768
    }
  );

  win.loadURL(`file://${__dirname}/index.html`);
}

function returnCPUInfo(event) {
  event.sender.send("cpu-count", require("os").cpus());
}

function returnNetworkInfo(event) {
  event.sender.send("network-info", require("os").networkInterfaces());
}
